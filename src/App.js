import React, { Component } from 'react'
import Navigation from './components/navigation';
import Header from './components/header'
import About from './components/about'
import Location  from './components/location'
import Test from './components/Test'
import Contact from './components/contact'
export class App extends Component {
  render() {
    return (
      <div>
        <Navigation /> 
        <Header/>
        <About/>
        <Location/>
        <Contact/>
      </div>
    )
  }
}

export default App;
