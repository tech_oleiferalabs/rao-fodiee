import React, { Component } from "react";
import { useFormik } from 'formik';
import axios from 'axios';
export class Contact extends React.Component {

 

  constructor(props) {
    super(props);


    this.state = {

      name:'k',
      email:'',
      subject:'',
      description:''
  
    }





    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.sendMessage = this.sendMessage.bind(this);

    
  }
  handleClick() {
   console.log("from handleClick");
  }

  handleChange(event123) {    
    var name = event123.target.name;
    var value = event123.target.value;
    //console.log(name +' - '+ value);

    //this.setState({name:value});
    this.setState({
      [name]: value
    });
  }

  sendMessage(){

      alert("from send message");

      console.log("------- test -------------------");
      console.log(this.state);


      axios.post('http://localhost:3456/contact', this.state)
      .then(function (response) {
        console.log(response);
      })

      /*axios('http://localhost:3456/contact',{
        method: 'POST',
        body: this.state,
        headers: {
         // 'Authorization': `bearer ${token}`,
         'Content-Type': 'application/json',
         'Access-Control-Allow-Origin':'*'
       }
      })
        .then(function(response) {
           console.log(response);
          }).then(function(body) {
            console.log(body);
          });
          */
      


  }


  render() {
    return (
      <div>
        <div id="contact">
          <div className="container">
            <div className="col-md-8">
              <div className="row">
                <div className="section-title">
                  <h2>Get In Touch</h2>
                  <p>
                    Please fill out the form below to send us an email and we
                    will get back to you as soon as possible.
                  </p>
                </div>
                
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <input
                          type="text"
                          id="name"
                          name="name"
                          className="form-control"
                          placeholder="Name"
                          required="required"
                          onChange={this.handleChange}
                          value={this.state.name}
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <input
                          type="text"
                          id="email"
                          name="email"
                          className="form-control"
                          placeholder="Email"
                          required="required"
                          onChange={this.handleChange}
                          value={this.state.email}
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <input
                          type="text"
                          id="phone"
                          className="form-control"
                          placeholder="Phone"
                          required="required"
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <input
                          type="text"
                          id="subject"
                          className="form-control"
                          placeholder="Subject"
                          required="required"
                          name="subject"
                          onChange={this.handleChange}
                          value={this.state.subject}
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <textarea
                      name="message"
                      id="message"
                      className="form-control"
                      rows="4"
                      name="description"
                      placeholder="Message"
                      onChange={this.handleChange}
                      value={this.state.description}
                      required
                    ></textarea>
                    <p className="help-block text-danger"></p>
                  </div>
                  <div id="success"></div>
                  <button onClick={this.sendMessage} className="btn btn-custom btn-lg">
                    Send Message
                  </button>
                
              </div>
            </div>
            <div className="col-md-3 col-md-offset-1 contact-info">
              <div className="contact-item">
                <h3>Contact Info</h3>
                <p>
                  <span>
                    <i className="fa fa-map-marker"></i> Address
                  </span>
                  {this.props.data ? this.props.data.address : "loading"}
                </p>
              </div>
              <div className="contact-item">
                <p>
                  <span>
                    <i className="fa fa-phone"></i> Phone
                  </span>{" "}
                  {this.props.data ? this.props.data.phone : "loading"}
                </p>
              </div>
              <div className="contact-item">
                <p>
                  <span>
                    <i className="fa fa-envelope-o"></i> Email
                  </span>{" "}
                  {this.props.data ? this.props.data.email : "loading"}
                </p>
              </div>
            </div>
            <div className="col-md-12">
              <div className="row">
                <div className="social">
                  <ul>
                    <li>
                      <a
                        href={this.props.data ? this.props.data.facebook : "/"}
                      >
                        <i className="fa fa-facebook"></i>
                      </a>
                    </li>
                    <li>
                      <a href={this.props.data ? this.props.data.twitter : "/"}>
                        <i className="fa fa-twitter"></i>
                      </a>
                    </li>
                    <li>
                      <a href={this.props.data ? this.props.data.youtube : "/"}>
                        <i className="fa fa-youtube"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="footer">
          <div className="container text-center">
            <p>
              &copy; 2020 Issaaf Kattan React Land Page Template. Design by{" "}
              <a href="http://www.templatewire.com" rel="nofollow">
                TemplateWire
              </a>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
