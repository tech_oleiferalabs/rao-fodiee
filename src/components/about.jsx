import axios from 'axios'
import React, { Component } from 'react'

export class about extends Component {

  state = {
    data:{}
  }

componentDidMount(){
  console.log("from about componentDidMount")
  axios.get('http://localhost:3456/about').then((res)=>{
  
    this.setState({
      data:res.data
    });
    console.log(this.state.data);
  });

}

  render() {
    return (
        <div id="about">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-6"> <img src="img/about.jpg" className="img-responsive" alt=""/> </div>
            <div className="col-xs-12 col-md-6">
              <div className="about-text">
                <h2>About Us</h2>
                <p>{this.state.data ? this.state.data.paragraph : 'loading... please wait'}</p>
                <h3>Why Choose Us?</h3>
                <div className="list-style">
                  <div className="col-lg-6 col-sm-6 col-xs-12">
                    <ul>
                      {this.state.data.Why ? this.state.data.Why.map((d, i) => <li  key={`${d}-${i}`}>{d}</li>) : 'loading'}
                    </ul>
                  </div>
                  <div className="col-lg-6 col-sm-6 col-xs-12">
                    <ul>
                    {this.state.data.why2 ? this.state.data.why2.map((d, i) => <li  key={`${d}-${i}`}> {d}</li>) : 'loading'}

                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default about
