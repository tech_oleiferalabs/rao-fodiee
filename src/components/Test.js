import axios from 'axios';
import React, { useState, useEffect } from 'react';

 
 function Test(){

    const [name, setName] = useState("");
    const[age,  setAge] = useState(0);
    const [email, setEmail] = useState("");


    //Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {

       // alert("From Test component useEffect");
       // res is a object, call back function
     axios.get('http://localhost:3456getPerson').then((res)=>{

     console.log(res);
     console.log(res.data.name);
    setName(res.data.name);
    setAge(res.data.age);
    setEmail(res.data.email);

     });


    });

    return(
        <>
        <h1>From Test component </h1>
        <h2>Hello my name is {name} my age is {age}</h2>
        <p>my Mail id is {email}</p>
        </>
    );



}
export default Test;